# frozen_string_literal: true

require_relative 'rkremap/version'
require_relative 'rkremap/keycode'
require_relative 'rkremap/winattr'
require_relative 'rkremap/evdev_list'
require_relative 'rkremap/evdev'
require_relative 'rkremap/event'
require_relative 'rkremap/uinput'
require_relative 'rkremap/remap'

class Rkremap
  include KeyCode

  # /usr/include/linux/input-event-codes.h
  EV_SYN = 0
  EV_KEY = 1
  EV_REL = 2
  EV_ABS = 3
  EV_MSC = 4
  SYN_REPORT = 0

  EVENT_TYPE_VALUE = {
    release: 0,
    press:   1,
    repeat:  2,
  }

  attr_accessor :grab
  attr_accessor :modifiers
  attr_accessor :x11
  attr_accessor :auto_detect
  attr_accessor :exclude
  attr_accessor :mouse

  # @param devices [Array<String>, String]
  # @param exclude [Regexp]
  # @param mouse [Boolean]
  def initialize(devices=[], exclude: nil, mouse: false)
    if devices.empty?
      @auto_detect = true
    else
      devices = Array(devices)
    end
    @devices = devices
    @exclude = exclude
    @mouse = mouse
    @uinput = Uinput.new
    @grab = false
    @x11 = false
    @modifiers = [
      KEY_LEFTCTRL,  KEY_RIGHTCTRL,
      KEY_LEFTSHIFT, KEY_RIGHTSHIFT,
      KEY_LEFTALT,   KEY_RIGHTALT,
      KEY_LEFTMETA,  KEY_RIGHTMETA,
    ]
    @event_procs = []
    @mutex = Mutex.new
  end

  # @overload remap(map)
  #   @param map [Hash] from => to
  # @overload remap(map:, hold: nil)
  #   @param map [Hash] from => to
  #   @param hold [Numeric] seconds
  def remap(**opts)
    map, hold = opts[:map], opts[:hold]
    map ||= opts
    Remap.new(self, map: map, hold: hold)
  end

  # @param device [Symbol, Regexp]
  # @param code [Integer]
  # @param type [Symbol] :press / :release / :repeat
  # @param app [Regexp, String]
  def match(device: nil, code: nil, type: nil, app: nil, &block)
    @event_procs.push [{device: device, code: code, type: type, app: app}, block]
  end

  def start(&block)
    @evdev_list = EvdevList.new(@devices, auto_detect: @auto_detect, exclude: @exclude, detect_mouse: @mouse)
    @evdev_list.grab = @grab
    @evdev_list.detect_loop
    @mod_state = @modifiers.map.to_h{|m| [m, false]}
    winattr = WinAttr.new if @x11
    @events = []
    while true
      @keys = []
      event = @events.shift || @evdev_list.read_event
      if event.ev_type != EV_KEY
        if event.device.grab?
          @uinput.write_event(event.ev_type, event.code, event.value)
        end
        next
      end
      app = App.new(winattr) if winattr
      event.app = app
      @event_procs.each do |cond, b|
        synchronize{ b.call(event, @mod_state.dup, app) } if event.match?(**cond)
        break if event.skipped?
      end
      next if event.skipped?
      synchronize{ proc_event(event, app) }
      @keys.each do |c, mod, app_|
        synchronize{ block.call(c, mod, app_) } if block
      end
    end
  end

  def synchronize(&block)
    @mutex.synchronize(&block)
  end

  # @param code [Integer]
  # @param type [Symbol] :press / :release / :repeat
  def event(code: nil, type: nil)
    value = EVENT_TYPE_VALUE[type] or raise "invalid type: #{type.inspect}"
    update_modifiers(code, value)
    write_event(code, value)
  end

  # @param code [Integer]
  # @param mod [Hash] {MOD_KEY => true, ...}
  def key(code, mod={})
    with_modifier(mod) do
      write_event(code, 1)
      write_event(code, 0)
    end
  end

  # @param mod [Hash] {MOD_KEY => true, ...}
  def with_modifier(mod, &block)
    mod_diff(mod).each do |mcode, state|
      write_event(mcode, state ? 1 : 0)
    end
    block.call
    mod_diff(mod).each_key do |mcode|
      write_event(mcode, @mod_state[mcode] ? 1 : 0)
    end
  end

  # @param event [Rkremap::Event]
  def append_event(event)
    @events.push event
    @events.push Event.new(event.device, event.time, EV_SYN, SYN_REPORT, 0)
  end

  private

  def update_modifiers(code, value)
    if @mod_state.include?(code)
      @mod_state[code] = value != 0
    end
  end

  # @param event [Rkremap::Event]
  # @param app [Rkremap::App]
  def proc_event(event, app)
    code, value = event.code, event.value
    if @mod_state.include?(code)
      write_event(code, value)
      update_modifiers(code, value)
    elsif value == 0
      write_event(code, value)
    else
      @keys.push [code, @mod_state.dup, app]
    end
  end

  def mod_diff(mod)
    mod.select{|k, v| @mod_state[k] != v}
  end

  # @param code [Integer]
  # @param value [Integer] 0:release / 1:press / 2:repeat
  def write_event(code, value)
    @uinput.write_event(EV_KEY, code, value)
  end

  class App
    # @param winattr [Rkremap::WinAttr]
    def initialize(winattr)
      @winattr = winattr
      @win = winattr.focus_win
    end

    # @return [String, nil]
    def class_name
      @winattr.app_win(@win)[2] if @winattr && @win
    end

    # @return [String, nil]
    def title
      @winattr.app_title(@win) if @winattr && @win
    end

    # @param app [Array, Hash, String, Regexp]
    # @return [Boolean]
    def match?(app)
      [app].flatten.each do |a|
        a = {class_name: a, title: a} unless a.is_a? Hash
        return true if match_sub(a[:class_name], class_name) || match_sub(a[:title], title)
      end
      false
    end

    # @param a [String, Regexp]
    # @param b [String]
    def match_sub(a, b)
      case a
      when String
        a == b
      when Regexp
        a =~ b
      else
        false
      end
    end
  end
end
