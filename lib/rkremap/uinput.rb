class Rkremap
  # https://www.kernel.org/doc/html/v4.12/input/uinput.html
  class Uinput
    UI_SET_EVBIT = 1074025828
    UI_SET_KEYBIT = 1074025829
    UI_SET_RELBIT = 1074025830
    UI_DEV_SETUP = 1079792899
    UI_DEV_CREATE = 21761

    REL_CNT = 0x10

    def initialize
      @dev = File.open('/dev/uinput', 'w')
      setup
    end

    def setup
      @dev.ioctl(UI_SET_EVBIT, EV_KEY)
      KeyCode::KEY_CNT.times{|k| @dev.ioctl(UI_SET_KEYBIT, k)}
      @dev.ioctl(UI_SET_EVBIT, EV_REL)
      REL_CNT.times{|r| @dev.ioctl(UI_SET_RELBIT, r)}
      bustype = 0x03    # BUS_USB
      vendor = 0x1234   # てきとー
      product = 0x5678  # てきとー
      version = 1       # てきとー
      name = 'rkremap'
      ff_efects_max = 0
      setup = [bustype, vendor, product, version, name, ff_efects_max].pack("SSSSZ80L") # struct uinput_setup
      @dev.ioctl(UI_DEV_SETUP, setup)
      @dev.ioctl(UI_DEV_CREATE)
    end

    # @param type [Integer] EV_KEY/ EV_SYN
    # @param code [Integer] キーコード / SYN_REPORT
    # @param value [Integer] 0:release / 1:press / 2:repeat
    def write_event(type, code, value)
      @dev.syswrite(['', type, code, value].pack('a16SSl'))
    end
  end
end
