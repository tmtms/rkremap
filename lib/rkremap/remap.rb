class Rkremap
  class Remap
    # @param map [Hash] from => to
    # @param hold [Numeric] seconds
    def initialize(rk, map:, hold:)
      @rk = rk
      @pressed = {}

      if hold
        setup_holding_event(map, hold)
      else
        setup_event(map)
      end
    end

    # @param map [Hash] {form => to}
    def setup_event(map)
      keycodes = map.keys.flatten

      @rk.match(code: keycodes, type: :press) do |event, current_mod|
        map.each do |from, to|
          if event_match?(from, event, current_mod)
            make_press_event(to, event)
            break
          end
        end
      end

      @rk.match(code: keycodes, type: :repeat) do |event|
        event.skip
      end

      @rk.match(code: keycodes, type: :release) do |event|
        if @pressed[event.code]
          @rk.event(code: @pressed[event.code], type: :release)
          @pressed[event.code] = nil
          event.skip
        end
      end
    end

    # @param map [Hash] {form => to}
    # @param hold [Numeric] seconds
    def setup_holding_event(map, hold)
      keycodes = map.keys.flatten
      timer = Tmtms::Timer.new
      jobs = map.transform_values{nil}

      @rk.match(code: keycodes, type: :press) do |event|
        map.each do |from, to|
          if from == event.code
            jobs[event.code] = timer.set(hold) do
              @rk.synchronize{ make_press_event(to) }
            end
            event.skip
            break
          end
        end
      end

      @rk.match(code: keycodes, type: :repeat) do |event|
        event.skip
      end

      @rk.match(code: keycodes, type: :release) do |event|
        code = event.code
        if jobs[code]
          if jobs[code].cancel
            event.value = 1
            @rk.append_event(Event.new(event.device, event.time, event.ev_type, event.code, 0))
          elsif map[code]
            event.code = map[code]
            @pressed[code] = nil
          end
          jobs[code] = nil
        end
      end

      @rk.match(type: :press) do
        jobs.each do |code, job|
          make_press_event(map[code]) if job&.cancel
        end
      end
    end

    # @param keys [Integer, Array<Integer>] KEY_A, [KEY_LEFTCTRL, KEY_A], [:SHIFT, KEY_A], ...
    # @param event [Rkremap::Event]
    # @param mod [Hash] current modifiers status
    def event_match?(keys, event, mod)
      if keys == event.code
        return true
      end
      keys = Array(keys)
      return false unless keys.include? event.code
      (keys - [event.code]).all?{mod[_1]}
    end

    # @param to [Integer, Array<Integer>]
    # @param event [Rkremap::Event, nil]
    def make_press_event(to, event=nil)
      to = Array(to)
      f = true
      to.each do |key|
        @pressed[event.code] = key if event
        if @rk.modifiers.include? key
          @rk.event(code: key, type: :press)
        elsif event
          event.code = key
          f = false
        else
          @rk.key(key)
          f = false
        end
      end
      event&.skip if f
    end
  end
end
