require 'tmtms/timer'

class Rkremap
  class EvdevList
    include Enumerable

    attr_reader :device_list
    attr_accessor :grab

    # @param device_files [Array<String>] path list (/dev/input/event*)
    # @param auto_detect [Boolean]
    # @param detect_mouse [Boolean]
    # @param exclude [Regexp]
    def initialize(device_files, auto_detect: false, detect_mouse: false, exclude: nil)
      @device_files = device_files
      @auto_detect = auto_detect
      @detect_mouse = detect_mouse
      @exclude = exclude
      @io2evdev = {}     # {IO => Evdev}
      @device_list = {}  # {path => Evdev}
      @prev_devices = []
    end

    def each(&block)
      @device_list.each_value do |evdev|
        block.call evdev
      end
    end

    def detect_loop
      detect
      timer = Tmtms::Timer.new
      timer.repeat(3) do
        detect
      end
    end

    def detect
      devs = Dir.glob("/dev/input/event*")
      (@device_list.keys - devs).each{|path| remove_device(path)}
      new_devs = devs - @prev_devices
      @prev_devices = devs
      new_devs &= @device_files unless @auto_detect
      new_devs -= @device_list.keys
      new_devs.each do |path|
        ev = Evdev.new(path)
        stat = nil
        if @exclude && ev.name =~ @exclude
          stat = 'excluded'
        elsif ev.keyboard? || (@detect_mouse && ev.mouse?)
          ev.grab if ev.match?(@grab)
          @io2evdev[ev.io] = ev
          @device_list[path] = ev
          stat = ev.grab? ? 'grabbed' : 'not grabbed'
        else
          stat = 'ignored'
        end
        puts "detect: #{ev.name} (#{ev.path}) #{stat}" if $VERBOSE
      end
    end

    # @param evdev [path]
    def remove_device(path)
      evdev = @device_list[path]
      puts "disconnect: #{evdev.name} (#{evdev.path})" if $VERBOSE
      @io2evdev.delete evdev.io
      evdev.close
      @device_list.delete path
    end

    # @param timeout [Numeric]
    # @return [Evdev]
    def select(timeout)
      r, = IO.select(@device_list.values.map(&:io), nil, nil, timeout)
      r && @io2evdev[r[0]]
    end

    def read_event
      while true
        evdev = select(3)
        next unless evdev
        begin
          return evdev.read_event
        rescue Errno::ENODEV
          remove_device(evdev.path)
        end
      end
    end
  end
end
