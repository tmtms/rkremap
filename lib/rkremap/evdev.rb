class Rkremap
  # https://www.kernel.org/doc/html/v4.12/input/input_uapi.html
  class Evdev
    EVIOCGBIT_ANY = 2147566880     # EVIOCGBIT(0, 1)
    EVIOCGBIT_EV_KEY = 2153792801  # EVIOCGBIT(EV_KEY, (KEY_CNT-1)/8+1)
    EVIOCGRAB = 1074021776
    EVIOCGNAME = 2164278534        # EVIOCGNAME(256)

    # @return [String]
    attr_reader :path

    # @param path [String]
    def initialize(path)
      @path = path
      @io = nil
      @capa = nil
      @uinput = nil
      @grab = false
    end

    def inspect
      "#<Rkremap::Evdev: #{name} (#{path})>"
    end

    def eql?(other)
      other.is_a?(Evdev) && self.path == other.path
    end

    # @return [IO]
    def io
      return @io if @io
      @io = File.open(@path)
    end

    # @return [String]
    def name
      return @name if @name
      buf = +''
      io.ioctl(EVIOCGNAME, buf)
      @name = buf.sub(/\0+$/, '')
    end

    def close
      @io&.close
    end

    # @param key [Integer]
    # @return [Boolean]
    def capable?(key)
      unless @capa
        buf = ' ' * ((KeyCode::KEY_MAX-1)/8+1)
        io.ioctl(EVIOCGBIT_EV_KEY, buf)
        @capa = buf.unpack('C*')
      end
      @capa[key/8][key%8] != 0
    end

    # @return [Boolean]
    def keyboard?
      return @is_keyboard unless @is_keyboard.nil?
      @is_keyboard = false
      return false if name =~ /\Arkremap\0*\z/

      buf = +''
      io.ioctl(EVIOCGBIT_ANY, buf)
      return false if buf.unpack1('C')[EV_KEY] == 0
      @is_keyboard = capable?(KeyCode::KEY_0) && capable?(KeyCode::KEY_9) &&
                     capable?(KeyCode::KEY_A) && capable?(KeyCode::KEY_Z) && capable?(KeyCode::KEY_SPACE)
    end

    # @return [Boolean]
    def mouse?
      return @is_mouse unless @is_mouse.nil?
      @is_mouse = false
      return false if name =~ /\Arkremap\0*\z/

      buf = +''
      io.ioctl(EVIOCGBIT_ANY, buf)
      return false if buf.unpack1('C')[EV_KEY] == 0
      @is_mouse = capable?(KeyCode::BTN_MOUSE)
    end

    def grab
      io.ioctl(EVIOCGRAB, 1)
      @grab = true
    end

    # @return [Boolean]
    def grab?
      @grab
    end

    # struct input_event {
    #   struct timeval time;
    #   unsigned short type;
    #   unsigned short code;
    #   unsigned int value;
    # };
    # @return [Rkremap::Event]
    def read_event
      raw = io.sysread(24)  # sizeof(struct input_event)
      sec, usec, type, code, value = raw.unpack('Q!Q!SSl')
      time = Time.at(sec, usec)
      Event.new(self, time, type, code, value)
    end

    # @param pattern [Symbol, Regexp]
    def match?(pattern)
      return true if pattern == true
      return true if pattern == :keyboard && keyboard?
      return true if pattern == :mouse && mouse?
      return true if pattern.is_a?(Regexp) && pattern =~ name
      return false
    end
  end
end
