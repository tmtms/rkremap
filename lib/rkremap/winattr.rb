require 'fiddle/import'

class Rkremap
  # アプリの name, class, title を取得する
  class WinAttr
    def initialize
      @app_win = {}
      @display = X11.XOpenDisplay(nil) or raise "Cannot open display: #{ENV['DISPLAY']}"
      @buf = ' '*8
    end

    # @return [Integer] Window
    def focus_win
      win = X11::Window.malloc(Fiddle::RUBY_FREE)
      X11.XGetInputFocus(@display, win, @buf)
      win.window
    end

    # @return [Array<window, name, class>]
    def app_win(window)
      return @app_win[window] if @app_win[window]
      class_hint = X11::XClassHint.malloc(Fiddle::RUBY_FREE)
      parent = X11::Window.malloc(Fiddle::RUBY_FREE)
      children = X11::Pointer.malloc(Fiddle::RUBY_FREE)
      win = window
      while win > 0
        class_hint.name = nil
        class_hint.class_name = nil
        X11.XGetClassHint(@display, win, class_hint)
        X11.XQueryTree(@display, win, @buf, parent, children, @buf)
        X11.XFree(children.ptr)
        break unless class_hint.name.null? && class_hint.class_name.null?
        win = parent.window
      end
      unless class_hint.name.null?
        win_name = class_hint.name.to_s
        X11.XFree(class_hint.name)
      end
      unless class_hint.class_name.null?
        win_class = class_hint.class_name.to_s
        X11.XFree(class_hint.class_name)
      end
      @app_win[window] = [win, win_name, win_class]
    end

    # @return [String]
    def app_title(window)
      win = app_win(window)[0]
      return '' if win == 0
      prop = X11::XTextProperty.malloc(Fiddle::RUBY_FREE)
      text_list = X11::Pointer.malloc(Fiddle::RUBY_FREE)
      X11.XGetWMName(@display, win, prop)
      X11.Xutf8TextPropertyToTextList(@display, prop, text_list, @buf)
      ptr = text_list.ptr
      return '' if ptr.null?
      title = ptr.ptr.to_s.force_encoding('utf-8')
      X11.XFreeStringList(text_list.ptr)
      title
    end
  end

  module X11
    extend Fiddle::Importer
    dlload 'libX11.so.6'
    typealias 'XID', 'unsigned long'
    typealias 'Window', 'XID'
    typealias 'Status', 'int'
    typealias 'Atom', 'unsigned long'
    Window = struct ['Window window']
    Pointer = struct ['void *ptr']
    XClassHint = struct ['char *name', 'char *class_name']
    XTextProperty = struct ['unsigned char *value', 'Atom encoding', 'int format', 'unsigned long nitems']

    extern 'Display* XOpenDisplay(char*)'
    extern 'int XGetInputFocus(Display*, Window*, int*)'
    extern 'int XGetClassHint(Display*, Window, XClassHint*)'
    extern 'Status XQueryTree(Display*, Window, Window*, Window*, Window**, unsigned int*)'
    extern 'Status XGetWMName(Display*, Window, XTextProperty*)'
    extern 'int Xutf8TextPropertyToTextList(Display*, XTextProperty*, char***, int*)'
    extern 'int XFree(void*)'
    extern 'void XFreeStringList(char**)'
  end
end
