class Rkremap
  class Event
    attr_accessor :app
    attr_reader :device
    attr_reader :time
    attr_reader :ev_type
    attr_accessor :code
    attr_accessor :value

    # @param evdev [Rkremap::Evdev]
    # @param time [Time]
    # @param type [Integer]
    # @param code [Integer]
    # @param value [Integer]
    def initialize(evdev, time, type, code, value)
      @device = evdev
      @time = time
      @ev_type = type
      @code = code
      @value = value
      @app = nil
      @skipped = false
    end

    def inspect
      if ev_type == EV_KEY
        "#<Rkremap::Event: #{code}(#{CODE_KEY[code]}) #{value} #{device.inspect}>"
      else
        "#<Rkremap::Event: type: #{ev_type}>"
      end
    end

    def type
      value == 1 ? :press : value == 0 ? :release : :repeat
    end

    def match?(device:, code:, type:, app:)
      (device.nil? || self.device.match?(device)) &&
        (code.nil? || Array(code).include?(self.code)) &&
        (type.nil? || Array(type).include?(self.type)) &&
        (app.nil? || @app.nil? || @app.match?(app))
    end

    def skip
      @skipped = true
    end

    def skipped?
      @skipped
    end
  end
end
