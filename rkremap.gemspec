# frozen_string_literal: true

require_relative "lib/rkremap/version"

Gem::Specification.new do |spec|
  spec.name = "rkremap"
  spec.version = Rkremap::VERSION
  spec.authors = ["TOMITA Masahiro"]
  spec.email = ["tommy@tmtm.org"]

  spec.summary = "key remapper"
  spec.description = "key remapper"
  spec.homepage = "https://gitlab.com/tmtms/rkremap"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/tmtms/rkremap"
  spec.metadata['documentation_uri'] = 'https://www.rubydoc.info/gems/rkremap'
  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.bindir = "exe"
  spec.executables = []
  spec.require_paths = ["lib"]
  spec.license = 'MIT'
  spec.files = ['README.md'] + Dir.glob('lib/**/*.rb') + Dir.glob('example/*.rb')

  spec.add_dependency 'tmtms_timer', '~> 0.3'
end
