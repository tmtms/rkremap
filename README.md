# Rkremap

Linux でキー割り当てを変更するプログラムを作るためのライブラリ。

## Installation

```
% gem install rkremap
```

## Usage

ライブラリなのでプログラムを自分で作る必要がある。具体的な使い方は `example/*.rb` を見ればよい。

`/dev/input/event*` と `/dev/uinput` に対する権限が必要。`sudo` 等で `root` 権限で動かすのが良いかも。

ユーザーを `input` グループに所属させると `/dev/input/event*` を読み取る権限をつけられるけど、そのユーザーはすべての入力デバイスの入力を読み取ることができるようになってしまうので、セキュリティ的にはビミョーなところ。

`input` グループでも `/dev/uinput` に権限はないけど、`/etc/udev/rules.d/input.rules` ファイルを
```
KERNEL=="uinput", GROUP="input"
```
という内容で作れば `/dev/uinput` のグループを `input` にできるっぽい。

`$VERBOSE` が真の場合はデバイスを検出したときと切断を検出したときにデバイス名を標準出力に出力する。

## Rkremap

### Rkremap.new

引数を指定しなければ自動的にキーボードデバイスの検出を試みる。

引数を指定する場合は `/dev/input/event*` を指定する。キーボードデバイス以外を指定してもたぶん無意味。
キーボードのデバイスファイル名を調べるには
```
cat /proc/bus/input/devices
```
とかすればわかるかもしれない。または
```
sudo evtest /dev/input/eventX
```
とかして何かキーを押して出力があればそれ。

ThinkPad 本体のキーボードは `/dev/input/event3` だった。

### Rkremap#exclude

無視するデバイス名を正規表現で指定できる。

例
```ruby
rkremap.exclude = /touchpad/i
```

### Rkremap#mouse

真を指定するとマウスも対象にする。

### Rkremap#grab

`true` を設定するとキーイベントを奪う。キーイベントを見るだけでいいなら `false` を設定する。

`:keyboard` を設定するとキーボードデバイスを grab する。

`:mouse` を設定するとマウスデバイスを grab する。

正規表現を設定するとデバイス名がマッチした場合にデバイスを grab する。

### Rkremap#x11

`true` に設定すると X のアプリ名等を取得できる。X 環境下でないなら `false` を設定する。

### Rkremap#modifers

修飾キーの一覧。これらのキーは単体で押されただけでは `start` ブロックを実行しない。

### Rkremap#auto_detect

`true` の場合はキーボードデバイスの接続を自動検知する。
`new` 時にデバイスファイルが指定されない場合は `true` になる。

### Rkremap#remap

引数の Hash で変換するキーを指定する:
```ruby
rk.remap(KEY_CAPSLOCK => KEY_LEFTCTRL)
```

変換元を配列で修飾キーを書くとその修飾キーとの組み合わせで変換する:
```ruby
rk.remap([KEY_LEFTCTRL, KEY_A] => KEY_HOME)
```

hold を指定すると指定秒数長押ししたときだけ変換する:
```ruby
rk.remap(hold: 0.5, map: {
  KEY_MUHENKAN => KEY_RIGHTMETA,
  KEY_HENKAN   => KEY_RIGHTMETA,
})
```

次のように書くと、A も B も C もすべて A になってしまう:
```ruby
rk.remap(KEY_A => KEY_B)
rk.remap(KEY_B => KEY_C)
rk.remap(KEY_C => KEY_A)
```

A を B, B を C, C を A と変換したい場合は次のように書く:
```ruby
rk.remap(
  KEY_A => KEY_B,
  KEY_B => KEY_C,
  KEY_C => KEY_A
)
```

`#remap` で実現できないような複雑な処理は、後述の `#match` や `#start` で書くことができるかもしれない。

### Rkremap#start

なにかキーを押されたらブロックを実行する。修飾キー単体ではブロックは実行されない。
ブロックの引数はキーコード(`Integer`)と修飾キー(`Hash`)と `Rkremap::App`。
`Rkremap::App#class_name` でアプリのクラス名、`Rkremap::App#title` でアプリのタイトルを取得できる。

### Rkremap#match

キーイベントが引数に適合するとブロックを実行する。ブロック引数は `Rkremap::Event` オブジェクト。
`Rkremap::Event#skip` するとこのイベントはスキップされる。

複数回指定した場合は記述順に実行される。

### Rkremap#key

キーを押したことにする。引数はキーコード(`Integer`)と修飾キー(`Hash`)。

修飾キーのハッシュのキーはキーコード(`KEY_LEFTCTRL` 等)、値は `true`, または `false`。

```ruby
{
  KEY_LEFTSHIFT => false,
  KEY_LEFTCTRL => true,
}
```

と指定すると、左シフトは離した状態、左Ctrlは押した状態、それ以外の修飾キーはそのまま。
`#key()` 処理後は処理前の修飾キーの状態に戻る。

### Rkremap#event

キーイベントを発生させる。`code` 引数はキーコード(`Integer`)。`type` 引数はイベントタイプ(`Symbol`)で `:press`, `:release`。

### Rkremap#synchronize

`start` や `match` のブロックは同時に実行されないようにするため Mutex を使用している。このメソッドで同じ Mutex を用いた排他制御を行う。

### Rkremap::CODE_KEY

コードからキー名のシンボルに変換するための Hash。

## Rkremap::Event

### Rkremap::Event#code

キーコード。`Rkremap::KeyCode::KEY_*` に該当。

### Rkremap::Event#type

イベントタイプ。 `:press`, `:release`, `:repeat` のいずれか。

### Rkremap::Event#app

`Rkremap::App`。イベントが発生したアプリケーション。

### Rkremap::Event#skip

このイベントをこれ以降処理しない。

## Rkremap::App

### Rkremap::App#class_name

アプリケーションのクラス名。

### Rkremap::App#title

アプリケーションのタイトル。

## 参考

* [mooz/xkeysnail: Yet another keyboard remapping tool for X environment](https://github.com/mooz/xkeysnail)
  * [xkeysnail - もうひとつの Linux 向けキーリマッパ - Qiita](https://qiita.com/mooz@github/items/c5f25f27847333dd0b37)
* [k0kubun/xremap: Dynamic key remapper for X11 and Wayland](https://github.com/k0kubun/xremap)
  * [Linux向けの最強のキーリマッパーを作った - k0kubun's blog](https://k0kubun.hatenablog.com/entry/xkremap)
  * [Linux用キーリマッパーxremapをRustで書き直した - k0kubun's blog](https://k0kubun.hatenablog.com/entry/xremap)
* [kui/rbindkeys: key remap with ruby](https://github.com/kui/rbindkeys)
  * [Ruby で設定できる Linux 環境向けキーリマッパー作った — 電卓片手に](http://k-ui.jp/blog/2012/06/06/rbindkeys-configurable-key-remapper-in-ruby/)
* [Linux の入力デバイスをカスタマイズ - Qiita](https://qiita.com/propella/items/a73fc333c95d14d06835)
* [Linux Input Subsystemの使い方](http://www.tatapa.org/~takuo/input_subsystem/input_subsystem.html)
* [1. Linux Input Subsystem userspace API — The Linux Kernel documentation](https://www.kernel.org/doc/html/v4.12/input/input_uapi.html)

## License

 MIT license
