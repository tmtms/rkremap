# 変換/無変換キーを0.5秒以上押してるとSUPERキーとして扱う。
# 0.5秒以内でも変換/無変換キーを押しながら他のキーを押すとSUPERキーとして扱う。

require 'rkremap'

include Rkremap::KeyCode

rk = Rkremap.new(*ARGV, mouse: true)
rk.grab = true

# HENKAN, MUHENKAN を 0.5秒以上押したら SUPER とする
rk.remap(hold: 0.5, map: {
  KEY_HENKAN => KEY_RIGHTMETA,
  KEY_MUHENKAN => KEY_RIGHTMETA,   # LEFTMETA だとデスクトップ環境のメニューが開いたりするため
})

# マウスボタンイベントはそのまま
rk.match(device: :mouse) do |event|
  unless Rkremap::CODE_KEY[event.code] =~ /^KEY_/
    rk.event(code: event.code, type: event.type)
    event.skip
  end
end

rk.start do |code, mod|
  rk.key(code, mod)
end
