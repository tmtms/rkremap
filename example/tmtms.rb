# tmtms 用

require 'rkremap'

include Rkremap::KeyCode

rk = Rkremap.new(*ARGV, mouse: true, exclude: /\AELAN0676/i)
rk.grab = true
rk.x11 = true

# rkremap がシステムに認識されたら中ボタンスクロールを有効にする
sleep 0.1 until system "xinput | grep -q rkremap"
system "xinput set-prop pointer:rkremap 'libinput Scroll Method Enabled' 0 0 1"

# HENKAN, MUHENKAN を 0.5秒以上押したら SUPER とする
rk.remap(hold: 0.5, map: {
  KEY_HENKAN => KEY_RIGHTMETA,
  KEY_MUHENKAN => KEY_RIGHTMETA,   # LEFTMETA だとデスクトップ環境のメニューが開いたりするため
})

# マウスボタンイベントはそのまま
rk.match(device: :mouse) do |event|
  unless Rkremap::CODE_KEY[event.code] =~ /^KEY_/
    rk.event(code: event.code, type: event.type)
    event.skip
  end
end

# CAPSLOCK は CTRL に変換
rk.remap(KEY_CAPSLOCK => KEY_LEFTCTRL)

# JISキーボードを英語配列にしたときに一部キーを変える
rk.remap(
  KEY_YEN => KEY_BACKSLASH,
  KEY_BACKSLASH => KEY_GRAVE,
  KEY_GRAVE => KEY_ESC,
  [KEY_LEFTSHIFT, KEY_RO] => [KEY_LEFTSHIFT, KEY_MINUS],
  [KEY_RIGHTSHIFT, KEY_RO] => [KEY_LEFTSHIFT, KEY_MINUS],
  KEY_RO => KEY_BACKSLASH,
)

mod_disable_ctrl = {
  KEY_LEFTCTRL => false,
  KEY_RIGHTCTRL => false,
}
mod_disable_shift = {
  KEY_LEFTSHIFT => false,
  KEY_RIGHTSHIFT => false,
}
mod_disable_alt = {
  KEY_LEFTALT => false,
  KEY_RIGHTALT => false,
}
mod_disable_meta = {
  KEY_LEFTMETA => false,
  KEY_RIGHTMETA => false,
}
mod_disable_all = {}.merge(mod_disable_ctrl, mod_disable_shift, mod_disable_alt, mod_disable_meta)

# VirtualBox
rk.match(app: /VirtualBox Machine/) do |event|
  rk.event(code: event.code, type: event.type)
  event.skip
end

# Emacs
rk.match(app: {class_name: 'Emacs'}) do |event, mod|
  # 修飾キーが Super だけの場合は Alt として扱う。ただしキーが TAB の場合は除く。
  mod_enabled = mod.select{_2}.keys
  if !mod_enabled.empty? && mod_enabled.all?{_1 == KEY_LEFTMETA || _1 == KEY_RIGHTMETA} && event.code != KEY_TAB
    rk.with_modifier(mod_disable_meta.merge(KEY_LEFTALT => true)) do
      rk.event(code: event.code, type: event.type)
    end
  else
    rk.event(code: event.code, type: event.type)
  end
  event.skip
end

# 端末
rk.match(app: /terminal/i) do |event, mod|
  # Super+Ctrl はそのまま
  if (mod[KEY_LEFTMETA] || mod[KEY_RIGHTMETA]) && (mod[KEY_LEFTCTRL] || mod[KEY_RIGHTCTRL])
    rk.event(code: event.code, type: event.type)
  # Super+0/Minus は Ctrl+0/Minus に変換
  elsif (mod[KEY_LEFTMETA] || mod[KEY_RIGHTMETA]) && Rkremap::CODE_KEY[event.code] =~ /\AKEY_(0|MINUS)\z/
    rk.with_modifier(mod_disable_all.merge(KEY_LEFTCTRL => true)) do
      rk.event(code: event.code, type: event.type)
    end
  # Super+KEY は Ctrl+Shift+KEY に変換
  elsif (mod[KEY_LEFTMETA] || mod[KEY_RIGHTMETA]) && Rkremap::CODE_KEY[event.code] =~ /\AKEY_([A-Z]|EQUAL)\z/
    rk.with_modifier(mod_disable_meta.merge(KEY_LEFTCTRL => true, KEY_LEFTSHIFT => true)) do
      rk.event(code: event.code, type: event.type)
    end
  # それ以外はそのまま
  else # rubocop:disable Lint/DuplicateBranch
    rk.event(code: event.code, type: event.type)
  end
  event.skip
end

# Firefox で Super+Ctrl+C は Alt+Ctrl+C に変換
rk.match(app: {class_name: 'firefox'}) do |event, mod|
  if (mod[KEY_LEFTMETA] || mod[KEY_RIGHTMETA]) && (mod[KEY_LEFTCTRL] || mod[KEY_RIGHTCTRL]) && event.code == KEY_C
    rk.with_modifier(mod_disable_meta.merge(KEY_LEFTCTRL => true, KEY_LEFTALT => true)) do
      rk.event(code: event.code, type: event.type)
    end
    event.skip
  end
end

# その他のウィンドウ
ctrl_map = {
  KEY_A => KEY_HOME,
  KEY_E => KEY_END,
  KEY_B => KEY_LEFT,
  KEY_F => KEY_RIGHT,
  KEY_N => KEY_DOWN,
  KEY_P => KEY_UP,
  KEY_D => KEY_DELETE,
  KEY_H => KEY_BACKSPACE,
  KEY_I => KEY_TAB,
}

rk.start do |code, mod|
  # Super + Ctrl + 何か はそのまま
  if (mod[KEY_LEFTMETA] || mod[KEY_RIGHTMETA]) && (mod[KEY_LEFTCTRL] || mod[KEY_RIGHTCTRL])
    rk.key(code, mod)
    next
  end

  # Super+[A-Z]/Minus/Equal/Enter を Ctrl+[A-Z]/Minus/Equal/Enter に変換
  if (mod[KEY_LEFTMETA] || mod[KEY_RIGHTMETA]) && (Rkremap::CODE_KEY[code] =~ /\AKEY_([0-9A-Z]|MINUS|EQUAL|ENTER)\z/)
    rk.key(code, mod.merge(mod_disable_meta, KEY_LEFTCTRL => true))
    next
  end
  # Super+[←→] を Alt+[←→] に変換
  if (mod[KEY_LEFTMETA] || mod[KEY_RIGHTMETA]) && (Rkremap::CODE_KEY[code] =~ /\AKEY_(LEFT|RIGHT)\z/)
    rk.key(code, mod.merge(mod_disable_meta, KEY_LEFTALT => true))
    next
  end

  # Alt + F, B は Ctrl + →, ← に変換
  if (mod[KEY_LEFTALT] || mod[KEY_RIGHTALT]) && (code == KEY_F || code == KEY_B)
    rk.key(code == KEY_F ? KEY_RIGHT : KEY_LEFT, mod.merge(mod_disable_alt, KEY_LEFTCTRL => true))
    next
  end

  # Ctrl は他のキーに変換
  if mod[KEY_LEFTCTRL] || mod[KEY_RIGHTCTRL]
    # Ctrl+I/O はそのまま
    if code == KEY_I || code == KEY_O
      rk.key(code, mod)
      next
    end

    # Ctrl+K は行末まで削除
    if code == KEY_K
      rk.key(KEY_END, mod_disable_all.merge(KEY_LEFTSHIFT => true)) # SHIFT+END
      rk.key(KEY_X, mod_disable_all.merge(KEY_LEFTCTRL => true))    # Ctrl+X
      next
    end

    # 単純な変換
    if ctrl_map[code]
      rk.key(ctrl_map[code], mod.merge(mod_disable_ctrl))
      next
    end

    # その他の Ctrl+[A-Z] は無視
    if Rkremap::CODE_KEY[code] =~ /\AKEY_[A-Z]\z/
      next
    end
  end

  # それ以外はそのまま
  rk.key(code, mod)
end
