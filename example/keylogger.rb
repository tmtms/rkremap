# 押されたキーを記録

require 'rkremap'

def code2key(code)
  Rkremap::CODE_KEY[code].to_s.sub(/\AKEY_/, '')
end

rk = Rkremap.new(*ARGV)
rk.grab = false
rk.x11 = true if ENV['DISPLAY']
rk.start do |code, mod, app|
  key = (mod.select{|_, v| v}.keys + [code]).map{|c| code2key(c)}.join('-')
  key << " at #{app.title} [#{app.class_name}]" if rk.x11
  puts key
end
